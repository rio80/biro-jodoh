@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card relative top-[-150px] z-10 rounded-2xl">

                <div class="card-body">
                    {{-- <div id="switch_text" {switchTextOptionLeft="Pasangan"} switchTextOptionLeft="Konsultasi"></div> --}}


                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="flex items-center justify-center">
                            <div id="switch-kategori"></div>
                        </div><br>

                        <div class="flex flex-col lg:flex-row gap-3 w-full justify-between">
                            <div class="flex flex-column gap-3 w-full">
                                <div class="flex justify-between">
                                    <div id="switch-gender"></div>
                                    <div class="flex flex-row justify-end">

                                        <label for="usia" class="col-md-3 col-form-label text-md-right">{{ __('Usia') }}</label>

                                        <div class="w-full">
                                            <input id="usia" type="number" class="form-control @error('usia') is-invalid @enderror" name="usia" value="{{ old('usia') }}" required autocomplete="usia" autofocus>

                                            @error('usia')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="flex justify-between gap-2">
                                    <label for="nama" class="mr-3 col-form-label">{{ __('Nama') }}</label>

                                    <div class="w-full">
                                        <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus>

                                        @error('nama')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="flex justify-between gap-2">
                                    <label for="select-lokasi" class="mr-3 col-form-label">{{ __('Lokasi') }}</label>
                                    <div id="select-lokasi" class="w-full"></div>
                                </div>

                            </div>


                            <div class="flex flex-column w-full gap-3">
                                <div class="flex flex-row justify-between">
                                    <label for="email" class="w-1/2 lg:w-3/4 col-form-label lg:text-right text-left">{{ __('Alamat Email') }}</label>

                                    <div class="w-full">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="flex flex-row justify-between">
                                    <label for="password" class="w-1/2 lg:w-3/4 col-form-label lg:text-right text-left">{{ __('Password') }}</label>

                                    <div class="w-full">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="flex flex-row w-full">
                                    <label for="password-confirm" class="w-1/2 lg:w-3/4 col-form-label lg:text-right text-left">{{ __('Confirm Password') }}</label>

                                    <div class="w-full">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="flex lg:flex-row flex-col w-full">
                                    <div class="flex flex-row gap-3 w-full">
                                        <input type="checkbox" name="approve_age" id="approve_age">
                                        <label for="approve_age" class="col-form-label text-md-left">Ya, saya sudah 18 tahun atau lebih dan menyetujui <b>Persyaratan Penggunaan</b> dan Pernyataan Privasi</label>
                                    </div>

                                    <div class="flex flex-row">
                                        <div class="w-full">
                                            <button type="submit" class="border-2 border-green-600 rounded text-lg text-black p-2 hover:text-white hover:bg-green-600 w-full">
                                                {{ __('Register') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
