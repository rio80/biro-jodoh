import React, { Component, useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { useCookies } from "react-cookie";
import { useRouter } from '../../hooks/useRouter';


function MainNavbar({page_type}) {

    const childContainer = React.createRef();
    const [cookies, setCookie, removeCookie] = useCookies();
    const router = useRouter();


    function toggleMobile() {
        return childContainer.current.classList.toggle("hidden");

    }

    function checkIdleness() {
        var t;
        window.onload = timerReset;
        window.onmousemove = timerReset;
        window.onmousedown = timerReset;  // catches touchscreen presses as well
        window.ontouchstart = timerReset; // catches touchscreen swipes as well
        window.onclick = timerReset;      // catches touchpad clicks as well
        window.onkeydown = timerReset;
        window.addEventListener('scroll', timerReset, true); // improved; see comments

        function writeYourFunction() {
            // your function for too long inactivity goes here
            removeCookie('auth_info', { path: '/' })
        }

        function timerReset() {
            console.log("has event");

            if (cookies.auth_info != undefined) {
                let expires = new Date()
                expires.setTime(expires.getTime() + (1 * 10000))
                setCookie('auth_info', "rio@gmail.com", { path: '/', expires });
            }
        }


    }

    checkIdleness();

    function setLogin(e) {
        // let expires = new Date()
        // expires.setTime(expires.getTime() + (1 * 60000))
        // setCookie('auth_info', "rio@gmail.com", { path: '/', expires });
      window.location.href = 'login';

    }

    function setRegister(e) {
      window.location.href = 'register';

    }

    function setLogout() {
        return removeCookie('auth_info');
    }

    return (
        <>
            <div className="absolute z-10 w-full">
                <nav className="bg-black-300 backdrop-filter backdrop-blur-xl bg-opacity-40 shadow-black">
                    <div className="max-w-6xl mx-auto px-4">
                        <div className="flex justify-end">
                            <div className="flex space-x-7 mr-10">
                                <div>
                                    {/* <!-- Website Logo --> */}
                                    {/* <a href="#" class="flex items-center py-4 px-2">
                                        <img src="logo.png" alt="Logo" class="h-8 w-8 mr-2" />
                                        <span class="font-semibold text-gray-500 text-lg">Navigation</span>
                                    </a> */}
                                </div>
                                {/* <!-- Primary Navbar items --> */}
                                <div className="hidden md:flex items-center space-x-10">
                                    <a href="" className="py-4 px-2 font-bold text-white hover:text-black-500 transition duration-300 ">Konsultasi</a>
                                    <a href="" className="py-4 px-2 font-bold text-white hover:text-black-500 transition duration-300">Artikel</a>
                                    <a href="" className="py-4 px-2 font-bold text-white hover:text-black-500 transition duration-300">Galeri</a>
                                    <a href="" className="py-4 px-2 font-bold text-white hover:text-black-500 transition duration-300">Tentang Kami</a>
                                </div>
                            </div>
                            {/* <!-- Secondary Navbar items --> */}
                            <div className="hidden md:flex items-center ml-10">
                                {
                                    cookies.auth_info == undefined ?
                                        <button onClick={page_type == 'Login' ? setLogin : setRegister} className="py-2 px-2 font-medium text-white rounded hover:bg-black-500 transition duration-300">{page_type}</button> :
                                        <button onClick={setLogout} className="py-2 px-2 font-medium text-white rounded hover:bg-black-500 transition duration-300">Logout</button>
                                }

                            </div>
                            {/* <!-- Mobile menu button --> */}
                            <div className="md:hidden flex items-center my-7">
                                <button className="outline-none mobile-menu-button transition-all ease-out duration-500" onClick={toggleMobile}>
                                    <svg className=" w-6 h-6 text-white "
                                        x-show="!showMenu"
                                        fill="none"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth="2"
                                        viewBox="0 0 24 24"
                                        stroke="currentColor"
                                    >
                                        <path d="M4 6h16M4 12h16M4 18h16"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                    {/* <!-- mobile menu --> */}
                    <div className="hidden mobile-menu" ref={childContainer}>
                        <ul className="duration-300 ease-out">
                            <li className="active"><a href="index.html" className=" font-semibold block font-semibold text-sm px-2 py-4 text-white">Home</a></li>
                            <li><a href="#services" className="block font-bold text-sm px-2 py-4 text-white">Services</a></li>
                            <li><a href="#about" className="block font-bold text-sm px-2 py-4 text-white">About</a></li>
                            <li><a href="#contact" className="block font-bold text-sm px-2 py-4 text-white">Contact Us</a></li>
                        </ul>
                    </div>
                    {/* <script>
				const btn = document.querySelector("button.mobile-menu-button");
				const menu = document.querySelector(".mobile-menu");

				btn.addEventListener("click", () => {
					menu.classList.toggle("hidden");
				});
			</script> */}
                </nav>
            </div>
        </>
    )
}

export default MainNavbar;
// const mainNavbar = document.getElementById("main_navbar");
// ReactDOM.render(<MainNavbar />, mainNavbar);

