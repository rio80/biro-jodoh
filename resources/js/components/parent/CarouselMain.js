import React, { Component, useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import ReactDOM from 'react-dom';

// Import Swiper styles


// import required modules
import { Autoplay, Navigation, Pagination } from "swiper";


class CarouselMain extends Component {
    render() {
        return (
            <>
                <div className="relative">
                <Swiper
                    pagination={{
                        clickable: true,
                    }}
                    autoplay={{
                        delay: 2500,
                        disableOnInteraction: false,
                    }}
                    loop={true}
                    modules={[Pagination, Autoplay, Navigation]}
                    className="mySwiper "
                    navigation={true}
                >
                    <SwiperSlide>
                        <img className="object-cover h-72 w-full" src="https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg"></img>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img className="object-cover h-72 w-full" src="https://upload.wikimedia.org/wikipedia/commons/f/f9/Phoenicopterus_ruber_in_São_Paulo_Zoo.jpg"></img>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img className="object-cover h-72 w-full" src="https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg"></img>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img className="object-cover h-72 w-full" src="https://upload.wikimedia.org/wikipedia/commons/f/f9/Phoenicopterus_ruber_in_São_Paulo_Zoo.jpg"></img>
                    </SwiperSlide>
                </Swiper>
                </div>
            </>
        );
    }

}

export default CarouselMain;

// const carouselMain = document.getElementById("carousel_main");
// ReactDOM.render(<CarouselMain />, carouselMain);
