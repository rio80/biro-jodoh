import React from 'react';
import Select from 'react-select';
import ReactDOM from 'react-dom';

export default function SelectParent({id, option, className}) {

    function logChange(val) {


        console.log("Selected: " + val);
    }


    return (
        <>
            <Select
                name={id}
                id={id}
                options={option}
                onChange={logChange}
                className={className}
            />
        </>
    )
}



