import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class SwitchText extends Component {
    constructor(props) {
        super(props);
    }
    render() {

        const {
            checked
            ,value
            ,onChange
            ,switchTextId
            ,switchTextOptionLeft
            ,switchTextOptionRight
            ,switchImageOptionLeft
            ,switchImageOptionRight
        } = this.props

        return (
            <>

                <input
                type="checkbox"
                id={switchTextId}
                className="checkbox"
                checked={checked}
                onChange={e => onChange(e.target.checked)}
                value={value}/>

                <label htmlFor={switchTextId} className="toggle" >
                    {
                        switchTextOptionLeft != undefined ?
                            <p>{switchTextOptionLeft}&nbsp;&nbsp;&nbsp;{switchTextOptionRight}</p>
                            :
                            <>
                                <div className='flex flex-row'>
                                    <img className='h-6 w-6 mt-0.5' src={switchImageOptionLeft} />
                                    <img className='h-6 w-6 mt-0.5' src={switchImageOptionRight} />
                                </div>
                            </>

                    }
                </label>

            </>
        )
    }
}

// const switchText = document.getElementById("switch_text");
// ReactDOM.render(<SwitchText/>, switchText);

export default SwitchText;
