import React, { Component, useState } from 'react';
import ReactDOM from 'react-dom';
import SwitchText from './parent/SwitchText';


export default function SwitchKategori() {

    let [checked, setChecked] = useState(false);

    let butuh =  (checked) ? "Pasangan" : "Konsultasi";

    return (
        <>
            <div className='flex flex-row gap-2'>
                <span className='font-bold text-lg'>Saya butuh </span>
                <SwitchText
                switchTextId='switchKategori'
                switchTextOptionLeft="Pasangan"
                switchTextOptionRight="Konsultasi"
                checked={checked}
                value={butuh}
                onChange={setChecked}>
                </SwitchText>
            </div>
        </>
    )
}


// const switchKategori = document.getElementById("switch-kategori");
// ReactDOM.render(<SwitchKategori />, switchKategori);
