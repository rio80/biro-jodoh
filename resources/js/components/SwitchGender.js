import React, { Component, useState } from 'react';
import ReactDOM from 'react-dom';
import maleIcon from "../../svg/male_1.svg";
import femaleIcon from "../../svg/female_8.svg";
import SwitchText from './parent/SwitchText';

export default function SwitchGender() {


    let [checked, setChecked] = useState(false);

    let gender = (checked) ? "Pria" : "Wanita";

    return (
        <>
            <div className='flex flex-row gap-2'>
                <span className='font-semibold text-sm leading-7'>Saya Adalah </span>
                <SwitchText
                switchTextId='switchGender'
                switchImageOptionLeft={maleIcon}
                switchImageOptionRight={femaleIcon}
                checked={checked}
                value={gender}
                onChange={setChecked}>
                </SwitchText>
            </div>
        </>
    )

}

// const switchGender = document.getElementById("switch-gender");
// ReactDOM.render(<SwitchGender />, switchGender);
