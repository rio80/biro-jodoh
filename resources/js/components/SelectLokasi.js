import React from 'react';
import ReactDOM from 'react-dom';
import SelectParent from './parent/SelectParent';

export default function SelectLokasi(){
    var option = [
        { value: "jakarta", label: "Jakarta" },
        { value: "bekasi", label: "Bekasi" },
        { value: "bogor", label: "Bogor" }
    ]
    return(
        <>
            <SelectParent id={'select-lokasi'} className="w-full" option={option}></SelectParent>
        </>
    );
}


// const selectLokasi = document.getElementById("select-lokasi");
// ReactDOM.render(<SelectLokasi/>, selectLokasi);
