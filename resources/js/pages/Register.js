import { Component } from "react";
import SwitchGender from "../components/SwitchGender";
import SwitchKategori from "../components/SwitchKategori";
import ReactDOM from 'react-dom';
import SelectLokasi from "../components/SelectLokasi";


class Register extends Component {

    handleSubmit(event) {
        event.preventDefault();
        console.log('Nama Kamu : ' + event.target.nama.value);
        console.log('Kelamin Kamu : ' + event.target.switchGender.value);

    }

    render() {
        return (
            <>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-9">
                            <div className="card relative top-[-150px] z-10 rounded-2xl">

                                <div className="card-body">
                                    <form onSubmit={this.handleSubmit}>

                                        <div className="flex items-center justify-center">
                                            <SwitchKategori></SwitchKategori>
                                        </div><br />

                                        <div className="flex flex-col lg:flex-row gap-3 w-full justify-between">
                                            <div className="flex flex-column gap-3 w-full">
                                                <div className="flex justify-between">
                                                    <SwitchGender></SwitchGender>
                                                    <div className="flex flex-row justify-end">

                                                        <label htmlFor="usia" className="col-md-3 col-form-label text-md-right">Usia</label>

                                                        <div className="w-full">
                                                            <input id="usia" type="number" className="form-control" name="usia" required autoComplete="usia" autoFocus />

                                                            <span className="invalid-feedback" role="alert">
                                                                {/* <strong>{{ $message }}</strong> */}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="flex justify-between gap-2">
                                                    <label htmlFor="nama" className="mr-3 col-form-label">Nama</label>

                                                    <div className="w-full">
                                                        <input id="nama" type="text" className="form-control" name="nama" required autoComplete="nama" autoFocus />

                                                        <span className="invalid-feedback" role="alert">
                                                            {/* <strong>{{ $message }}</strong> */}
                                                        </span>
                                                    </div>
                                                </div>

                                                <div className="flex justify-between gap-2">
                                                    <label htmlFor="select-lokasi" className="mr-3 col-form-label">Lokasi</label>
                                                    <SelectLokasi></SelectLokasi>
                                                </div>

                                            </div>


                                            <div className="flex flex-column w-full gap-3">
                                                <div className="flex flex-row justify-between">
                                                    <label htmlFor="email" className="w-1/2 lg:w-3/4 col-form-label lg:text-right text-left">Email</label>

                                                    <div className="w-full">
                                                        <input id="email" type="email" className="form-control" name="email" required autoComplete="email" />

                                                        <span className="invalid-feedback" role="alert">
                                                            {/* <strong>{{ $message }}</strong> */}
                                                        </span>
                                                    </div>
                                                </div>

                                                <div className="flex flex-row justify-between">
                                                    <label htmlFor="password" className="w-1/2 lg:w-3/4 col-form-label lg:text-right text-left">Password</label>

                                                    <div className="w-full">
                                                        <input id="password" type="password" className="form-control" name="password" required autoComplete="new-password" />

                                                        <span className="invalid-feedback" role="alert">
                                                            {/* <strong>{{ $message }}</strong> */}
                                                        </span>
                                                    </div>
                                                </div>

                                                <div className="flex flex-row w-full">
                                                    <label htmlFor="password-confirm" className="w-1/2 lg:w-3/4 col-form-label lg:text-right text-left">Confirm Password</label>

                                                    <div className="w-full">
                                                        <input id="password-confirm" type="password" className="form-control" name="password_confirmation" required autoComplete="new-password"  />
                                                    </div>
                                                </div>

                                                <div className="flex lg:flex-row flex-col w-full">
                                                    <div className="flex flex-row gap-3 w-full">
                                                        <input type="checkbox" name="approve_age" id="approve_age" />
                                                        <label htmlFor="approve_age" className="col-form-label text-md-left">Ya, saya sudah 18 tahun atau lebih dan menyetujui <b>Persyaratan Penggunaan</b> dan Pernyataan Privasi</label>
                                                    </div>

                                                    <div className="flex flex-row">
                                                        <div className="w-full">
                                                            <button type="submit" className="border-2 border-green-600 rounded text-lg text-black p-2 hover:text-white hover:bg-green-600 w-full">
                                                                Register
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Register;
// if (document.getElementById('register_page')) {
//     ReactDOM.render(<Register />, document.getElementById('register_page'));
// }
