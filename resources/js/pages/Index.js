import React, { useState } from "react";
import ReactDOM from 'react-dom';
import CarouselMain from "../components/parent/CarouselMain";
import MainNavbar from "../components/parent/Navbar";
import { BrowserRouter as Router } from 'react-router-dom';
import Register from "./Register";
import Login from "./Login";


function Index() {

    const [page, setPage] = useState('login');

    console.log(window.location.pathname);

    useState(() =>{
        if(window.location.pathname == '/login'){
            setPage('Register');
        }else{
            setPage ('Login');

        }
    })
    function getPage(){

        return window.location.pathname == '/login' ?
        <Login></Login>:
        <Register></Register>;
    }

    return (
        <>
            <Router>
                <MainNavbar page_type={page}></MainNavbar>
                <CarouselMain></CarouselMain>
                {getPage()}

            </Router>

        </>
    )
}

// export default Index;

if (document.getElementById('index')) {
    ReactDOM.render(<Index />, document.getElementById('index'));
}
