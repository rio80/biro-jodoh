<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('nama_lengkap', 150)->nullable();
            $table->string('nama_panggilan', 50)->nullable();
            $table->string('nik', 18)->nullable();
            $table->string('kelamin', 12)->nullable();
            $table->string('lokasi', 100)->nullable();
            $table->integer('usia')->nullable();
            $table->string('suku', 100)->nullable();
            $table->string('gelar_bangsawan', 100)->nullable();
            $table->string('gelar_akademik', 100)->nullable();
            $table->string('nama_ayah', 100)->nullable();
            $table->string('suku_ayah', 100)->nullable();
            $table->string('suku_ibu', 100)->nullable();
            $table->string('telp', 20)->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
