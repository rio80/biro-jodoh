<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends BaseController
{

    public function IndexRegister(){
        return view('home');

    }
    public function Register(Request $request){
        $validator = Validator::make($request->all(), [
            'nama' => ['required', 'string', 'max:40', 'min:3'],
            'email' => ['required','string','max:100'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $message = array();
        if ($validator->fails())
        {
            $message = $validator->errors()->all();
        }

        $registerData = [
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'is_login' => 1
        ];

        $user = User::create($registerData);

        $user = User::where('email', $request->email)->first();

        event(new Registered($user));

        Auth::guard()->login($user);

        return redirect(RouteServiceProvider::HOME);
    }

    public function IndexLogin(Request $request){
        return view('home');
    }

    public function Login(Request $request){
    }
}
